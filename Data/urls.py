from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name="index"),
    path('smoi/', views.UwU, name="somi"),
    path('OwO/<str:url_ID>', views.OwO, name="OwO"),
]
