from django.db import models

# Create your models here.

class URL(models.Model):
    URL_ID = models.AutoField(primary_key=True)
    URL_name = models.CharField(max_length=10)
    URL_link = models.CharField(max_length=25)
